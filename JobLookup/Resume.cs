﻿using System;
using System.Data.Entity;

namespace JobLookup
{
   public class Resume
    {
        public int id { get; set; }
        public string JobName { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Gender { get; set; }
        public string Salary { get; set; }
        public string Experience { get; set; }
        public string Education { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public bool HasPhoto { get; set; }
        public byte[] ImageData { get; set; }
        public string url { get; set; }
    }

   public class ResumeContext : DbContext
    {
       public DbSet<Resume> Resumes { get; set; }
    }
}
