﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;

namespace JobLookup
{
    public partial class ResumeSummary : Page
    {
        public ResumeSummary()
        {
            InitializeComponent();
        }
        public ResumeSummary(object data)
            : this()
        {
            this.DataContext = data;
            Resume thisresume = (Resume)data;
            this.Mylink.NavigateUri = new Uri("http://rabota.e1.ru/" + thisresume.url);
        }

        void HandleRequestNavigate(object sender, RoutedEventArgs e)
        {
            string navigateUri = this.Mylink.NavigateUri.ToString();
            Process.Start(new ProcessStartInfo(navigateUri));
            e.Handled = true;
        }
     
    }
}
