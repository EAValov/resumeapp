﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace JobLookup
{
    /// <summary>
    /// Логика взаимодействия для Home.xaml
    /// </summary>
    public partial class Home : Page
    {
       public List<string> categories = new List<string>();

       public Home()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(HomePage_Loaded);
            this.ViewCategoryButton.Visibility = System.Windows.Visibility.Hidden;
            this.DownloadButton.IsEnabled = false;
        }
    

        static void Insert(Resume resume)
        {
            using (var db = new ResumeContext())
            {
                db.Resumes.Add(resume);
                db.SaveChanges();
            }
        }

        static bool TryGetPhoto(string url, ref Resume resume)
        {
            var webClient = new WebClient();
            try
            {
                resume.ImageData = webClient.DownloadData(url);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ResumeList resumelist = new ResumeList(this.ResumeCategoryListBox.SelectedItem);
            this.NavigationService.Navigate(resumelist);
        }


        private void HomePage_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshCategoryList();
            this.ResumeCategoryListBox.ItemsSource = categories;
            if (categories.Count == 0) { this.DownloadButton.IsEnabled = true; }
            if (!this.DownloadButton.IsEnabled) { this.DownloadButton.Content = "Downloading..."; }
        }

        private void RefreshCategoryList()
        {
            using (var context = new ResumeContext())
            {
                categories.Clear();
                categories.AddRange(context.Resumes.Select(y => y.Category).Distinct());
            }
        }

        private void ResumeCategoryListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ViewCategoryButton.Visibility = System.Windows.Visibility.Visible; 
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.DownloadButton.IsEnabled = false;

            Task.Factory.StartNew(() => //this will start another tread 
            {
                int totalResumes;
                WebClient client = new WebClient();
                dynamic metadata = client.DownloadString("http://rabota.e1.ru/api/v1/resumes/");
                JObject MyJson = JObject.Parse(metadata);
                bool ResumesParsed = int.TryParse(MyJson["metadata"]["resultset"]["count"].ToString(), out totalResumes); //parse metadata to get total resume count
                if (ResumesParsed)
                {
                    for (int i = 0; i < totalResumes; i = i + 50)
                    {
                        string url = string.Format("http://rabota.e1.ru/api/v1/resumes/?limit=50&offset={0}", i);
                        dynamic data = client.DownloadString(url);
                        JObject JsonResult = JObject.Parse(data);
                        JArray listresumes = JArray.Parse(JsonResult["resumes"].ToString());

                        foreach (var resume in listresumes)
                        {
                            try
                            {
                                Resume Newresume = new Resume()
                                {
                                    JobName = resume["header"].ToString(),
                                    Birthday = ((resume["birthday"].Type != JTokenType.Null) ? DateTime.Parse(resume["birthday"].ToString(), new CultureInfo("en-US")) : (DateTime?)null),
                                    Gender = ((resume["sex"].Type != JTokenType.Null) ? resume["sex"].ToString() : null),
                                    Salary = ((resume["salary"].Type != JTokenType.Null) ? resume["salary"].ToString() : null),
                                    Experience = ((resume["experience_length"].Type != JTokenType.Null) ? resume["experience_length"]["title"].ToString() : null),
                                    Education = ((resume["education"].Type != JTokenType.Null) ? resume["education"]["title"].ToString() : null),
                                    Category = ((resume["rubrics"].Count() != 0) ? resume["rubrics"][0]["title"].ToString() : null),
                                    Description = Regex.Replace(resume["experience"].ToString(), @"<[^>]+>", "").Trim(), // gegex to get rid of HTML markup
                                    url = resume["url"].ToString(),
                                };

                                if (resume["photo"]["url"].Type != JTokenType.Null)
                                {
                                    Newresume.HasPhoto = TryGetPhoto("http://rabota.e1.ru/" + resume["photo"]["url"].ToString(), ref Newresume);
                                }

                                var FirstName = ((resume["contact"]["firstname"].Type != JTokenType.Null) ? resume["contact"]["firstname"].ToString() : string.Empty);
                                var LastName = ((resume["contact"]["lastname"].Type != JTokenType.Null) ? resume["contact"]["lastname"].ToString() : string.Empty);
                                var Patronymic = ((resume["contact"]["patronymic"].Type != JTokenType.Null) ? resume["contact"]["patronymic"].ToString() : string.Empty);

                                //not very beautiful ;(
                                //but it works :)
                                if (Newresume.Birthday.HasValue)
                                {
                                    int year;
                                    int years;
                                    year = Newresume.Birthday.Value.Year;
                                    years = DateTime.Now.Year - year;
                                    string fullname = string.Format("{0} {1} {2} {3} лет", FirstName, Patronymic, LastName, years);
                                    Newresume.FullName = fullname;
                                }
                                else
                                {
                                    string fullname = string.Format("{0} {1} {2}", FirstName, Patronymic, LastName);
                                    Newresume.FullName = fullname;
                                }

                                Insert(Newresume);
                            }
                            catch (Exception)
                            {
                                System.Diagnostics.Debug.WriteLine("Something go wrong!");
                            }
                        }
                        //update listbox
                        RefreshCategoryList();
                        this.Dispatcher.Invoke((Action)(() => {
                            ResumeCategoryListBox.Items.Refresh();
                            MyGird.UpdateLayout();
                        })); ;
                    }
                }
                this.DownloadButton.Content = "Download Complete";
            });
        }
    }
}
