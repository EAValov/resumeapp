﻿using System;
using System.Data.Entity;
using System.Windows.Navigation;

namespace JobLookup
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
            this.Source = new Uri("Home.xaml", UriKind.Relative);
            //if you want to reuse previously downloaded resumes, comment line below
            Database.SetInitializer(new DropCreateDatabaseAlways<ResumeContext>());
        }   

    }

}
