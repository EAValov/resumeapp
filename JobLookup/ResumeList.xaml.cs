﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace JobLookup
{
    /// <summary>
    /// Логика взаимодействия для ResumeList.xaml
    /// </summary>
    public partial class ResumeList : Page
    {
        private string category;
        private bool hasPhoto;
        private string education;
        private string experience;

        private string gender;
        public List<Resume> resumes;
        public List<Resume> resumesToDisplay;
        public ResumeList()
        {
            InitializeComponent();
        }

        public ResumeList(object data)
            : this()
        {
            this.category = data.ToString();
            using (var context = new ResumeContext())
            {
                this.resumes = new List<Resume>(context.Resumes.Where(u => u.Category == category));
            }
            Loaded += new RoutedEventHandler(ResumeList_Loaded);
            this.ResumeSummary.IsEnabled = false;
            ResumeListBox.ItemsSource = this.resumesToDisplay;
        }

        private void ResumeList_Loaded(object sender, RoutedEventArgs e)
        {
            //default settings
            this.PhotoCheckBox.IsChecked = false;
            this.MaleRadioButton.IsChecked = false;
            this.FemaleRadioButton.IsChecked = false;
            this.EducationComboBox.Text = "Education";
            this.ExperienceComboBox.Text = "Experience";
            ResumeListBox.ItemsSource = this.resumes; //modelBinding
            EducationComboBox.ItemsSource = this.resumes.Select(q => q.Education).Distinct();
            ExperienceComboBox.ItemsSource = this.resumes.Select(h => h.Experience).Distinct();

        }

        private void PhotoCheckBox_Checked_1(object sender, RoutedEventArgs e)
        {
            if (PhotoCheckBox.IsChecked == true)
                this.hasPhoto = true;
            else
                this.hasPhoto = false;
            Reselect();
        }

        private void Reselect()
        {
            if (PhotoCheckBox.IsChecked == true)
                this.resumesToDisplay = new List<Resume>(this.resumes.Where(r => r.HasPhoto == this.hasPhoto));
            else
                this.resumesToDisplay = new List<Resume>(this.resumes);

            if (this.education != null) { this.resumesToDisplay = this.resumesToDisplay.Where(r => r.Education == education).ToList(); }
            if (this.experience != null) { this.resumesToDisplay = this.resumesToDisplay.Where(s => s.Experience == experience).ToList(); }
            if (this.gender != null) { this.resumesToDisplay = this.resumesToDisplay.Where(o => o.Gender == this.gender).ToList(); }
            ResumeListBox.ItemsSource = this.resumesToDisplay;
            ResumeListBox.UpdateLayout();
        }

        private void ExperienceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

           if(ExperienceComboBox.SelectedItem != null) this.experience = ExperienceComboBox.SelectedItem.ToString();
            Reselect();
        }

        private void EducationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(this.EducationComboBox.SelectedItem != null) this.education = EducationComboBox.SelectedItem.ToString();
            Reselect();
        }

        private void MaleRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (MaleRadioButton.IsChecked == true) { this.gender = "male"; }
            Reselect();
        }

        private void FemaleRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (FemaleRadioButton.IsChecked == true) { this.gender = "female"; }
            Reselect();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ResumeSummary summary = new ResumeSummary(this.ResumeListBox.SelectedItem);
            this.NavigationService.Navigate(summary);
        }

        private void ResumeListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ResumeSummary.IsEnabled = true;
        }

        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new ResumeContext())
            {
                this.resumes = new List<Resume>(context.Resumes.Where(u => u.Category == category));
            }
            Reselect();
        }
    }
}
